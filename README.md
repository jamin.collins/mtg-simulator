# Purpose

Statistical sampling of opening hands for Magic the Gathering decks through brute force iterations.

# Usage

The only deck currently supported is **Belcher**.

```
./belcher-sim.py 100 decklists/belcher/BelcherSimDecklist-serum-x4
```

# Why

Sometimes you just want to know how likely it is that you can get a specific sequence of cards in your opening hand.

# TODO

Sometime in the future I plan to add support for other decks, such as:
* Oops, all Spells
