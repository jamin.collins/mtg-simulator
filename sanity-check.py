#!/usr/bin/env python3

import os
import sys

decklist_dir = os.path.join("decklists", sys.argv[1])
for entry in os.listdir(decklist_dir):
    total = 0
    filename = os.path.join(decklist_dir, entry)
    with open(filename) as handle:
        contents = handle.read()
    for line in contents.splitlines():
        if line.strip() == "":
            continue
        parts = line.split()
        total += int(parts[0])
    print(entry, total)
