#!/usr/bin/env python3
"""
Usage:
    {0} <iterations> <decklist> [options]

Options:
"""

import random
import os
import sys

import multiprocessing
from multiprocessing import Pool

import multiset

import functools
from collections import OrderedDict

from docopt import docopt


@functools.cache
def parse_desirable_hands():
    desirable_hands = []
    desirable_hands = OrderedDict()
    hands_dir = os.path.join("hands", "belcher")
    for entry in os.listdir(hands_dir):
        hands_name = entry[3:]
        desirable_hands[hands_name] = []
        filename = os.path.join(hands_dir, entry)
        with open(filename) as handle:
            contents = handle.read()
        for line in contents.splitlines():
            if line.startswith("#"):
                continue
            if line.strip() == "":
                continue
            cards = line.split()
            hand = multiset.Multiset()
            for card in cards:
                hand.add(card)
            desirable_hands[hands_name].append(hand)
    #print(desirable_hands.keys())
    return desirable_hands


#@functools.cache
def parse_deck(decklist):
    deck = []
    with open(decklist) as handle:
        contents = handle.read()
    for line in contents.splitlines():
        # strip everything after the % in the decklist
        line = line[0:line.find("%")].strip()
        count, card = line.split()
        for i in range(int(count)):
            deck.append(card)
    #print(deck)
    #print(len(deck))
    return deck


@functools.cache
def get_min_hand_size():
    min_hand_size = []
    desirable_hands = parse_desirable_hands()
    for hand_type in desirable_hands:
        min_hand_size.append(min(map(len, desirable_hands[hand_type])))
    min_desirable_hand_size = min(min_hand_size)
    return min_desirable_hand_size


def is_desirable(hand):
    desirable_hands = parse_desirable_hands()
    for hand_type in desirable_hands:
        for desirable_hand in desirable_hands[hand_type]:
            #print(f"desired -- {sorted(desirable_hand)}")
            #print(f"hand -- {sorted(hand)}")
            if desirable_hand.issubset(hand):
                return hand_type, True
    return None, False


def get_desired_hand():
    desirable_hands = parse_desirable_hands()
    hand = random.choice(desirable_hands)
    return hand


def draw_hand(deck, hand_size=7, shuffle=True):
    if shuffle:
        random.shuffle(deck)
    hand = multiset.Multiset()
    #hand = get_desired_hand()
    while len(hand) < hand_size:
        hand.add(deck.pop())
    return hand


def select_cards_to_put_back(hand, deck, num_to_bottom):
    # TODO: this needs more logic to select the best cards to put back
    cards_to_bottom = []
    count = 0
    while len(cards_to_bottom) < num_to_bottom:
        card = random.choice(hand)
        cards_to_bottom.append(card)
    return cards_to_bottom


def starting_hand(deck):
    min_hand_size = get_min_hand_size()

    mulligan_count = 0
    max_mulligan = 7 - min_hand_size
    shuffle_required = True
    draw_size = 7
    while mulligan_count <= max_mulligan:
        hand = draw_hand(deck, hand_size=draw_size, shuffle=shuffle_required)
        hand_type, desirable = is_desirable(hand)
        if desirable:
#            print(f"mulligan_count: {mulligan_count}, hand: {hand}")
            return hand_type
        # if we got here, handle mulligans
        if "Z" in hand:
            shuffle_required = False
#            print("Serum Powder found")
            # mulligan_count cards need to go back into the deck
            cards_to_bottom = select_cards_to_put_back(hand, deck, mulligan_count)

            pass
            # if Serum Power is in our opening hand, we can exile cards instead
            # electing to use the Serum Powder removes the hand from the game
        else:
            mulligan_count += 1
            draw_size = 7
            shuffle_required = True
            # for normal mulligans the cards are put back into the deck
            for entry in hand:
                deck.append(entry)
    return hand_type


def parse_cmdline():
    args = docopt(
        __doc__.format(
            os.path.basename(__file__)
        )
    )
    return args


def main():
    args = parse_cmdline()
    decklist = args['<decklist>']
    iterations = int(args['<iterations>'])
    count = 0
    counts = {}
    # TODO: split this and multi-thread the work?
    for i in range(1, iterations + 1):
        #if i % 1000 == 0:
        #    print(f"iterations = {i}")
        deck = parse_deck(decklist)
        hand_type = starting_hand(deck)
        if hand_type:
            if not hand_type in counts:
                counts[hand_type] = 0
            counts[hand_type] += 1
    print(f"iterations = {i}")
    hand_types = parse_desirable_hands().keys()
    print(f"Decklist: {os.path.basename(decklist)}")
    for hand_type in hand_types:
        if not hand_type in counts:
            counts[hand_type] = 0
        count += counts[hand_type]
        odds = round((count/iterations) * 100, 2)
        print(f"{hand_type} success count: {counts[hand_type]}")
        print(f"Win by {hand_type} odds: {odds}%")
    print(f"Overall odds - {odds}%")
    exit()


if __name__ == '__main__':
    main()
